# One website for many earthquake resources #

Some institutes around the world allow one to query their database and get a list of eartquake in QuakeML format. This application is a middleware to query the resources of the institutes USGS https://www.usgs.gov/, INGV http://www.ingv.it/, RENASS http://renass.unistra.fr/, ISC http://www.isc.ac.uk/, and ETHZ http://www.seismo.ethz.ch/en/home/.

Moreover, if there is a relevant disaster alert by the GDACS (Global Disaster Alert and Coordination System) the website will pull the maps from http://www.gdacs.org/.

# How it looks like #

A screenshot of the website: https://bitbucket.org/troquard/terremotoquakeml/src/master/screenshot.jpeg.


# Options #

TER/app?&s=it to use the resources INGV, TER/app?&s=fr to use the resources RENASS, TER/app?&s=ch to use the resources ETHZ, TER/app?&s=uk to use the resources ISC, TER/app?&s=us to use the resources USGS (by default: us)

TER/app?&m=5.8 to see the earthquakes of magnitude over 5.8 (by default: 4.5) (note: some resources do not show earthquakes of low magnitude in some countries)

TER/app?&r=55.0 to see the earthquakes in a radius of 55.0 km (by default: 100.0)

TER/app?&l="san francisco" to see the earthquakes around San Francisco (by default: Colledara)

TER/app?&map=t to see the earthquakes on the map (by default: no map)

TER/app?&start=2016-10-21&end=2016-10-23 to see the earthquakes between two dates (by default: earthquakes occuring recently)

Combine these options without any particular order: TER/app?&l="san francisco"&m=5.6&map=t

# Setup with Apache #

Make sure that apache2 is installed on your system. E.g.,
```
apt-get install apache2
```
Make sure you have the required python modules. E.g.,
```
apt-get install python-lxml python-geopy
```

Enable cgi. Add the following line to the apache config file.
```
LoadModule cgid_module modules/mod_cgid.so
```
Run
```
sudo a2enmod cgi
sudo apache2ctl restart
```

Place the TER/ directory in /usr/lib/cgi-bin/.

Point your browser to localhost/cgi-bin/TER/app, which should show you a page of the recent earthquakes in centre Italy; hopefully none. Read the information on the page to change the options.

The option showing maps uses the Google Static Map API . See https://developers.google.com/maps/documentation/static-maps/ and https://developers.google.com/maps/documentation/static-maps/intro. It requires identification and so a key must be provided. A key can be obtained here https://developers.google.com/maps/documentation/static-maps/get-api-key. Then, in the file app, replace API_KEY="Your API key here" with your API key.

By editing the file app, you can also change the language and the default behaviours.
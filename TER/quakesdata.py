import urllib2
import datetime

import conveniencefunctions as cf
import resources as r


def pull_geojson(url, SITE):
    import json
    try:
        response=urllib2.urlopen(url)
    except urllib2.HTTPError:
        return []
    data = json.loads(response.read())
    quakes = []
    for e in data['features']:
        # magnitude
        magnitude=e.get('properties').get('mag')
        # where
        place=e.get('properties').get('place')
        latitude=e.get('geometry').get('coordinates')[1]
        longitude=e.get('geometry').get('coordinates')[0]
        # when
        if r.PARAMS[SITE]["Xtimeformatgeojson"] == "nummillisec":
            timestamp=e.get('properties').get('time') / 1e3
        else:
            timestamp=(datetime.datetime.strptime(
                e.get('properties').get('time')
                , r.PARAMS[SITE]["Xtimeformatgeojson"])
                       - datetime.datetime(1970,1,1)).total_seconds()
        time = datetime.datetime.utcfromtimestamp(timestamp)
        # # quake url if exists
        # if r.PARAMS[SITE]['Xurl'] != None:
        #     quake_url=(e.get('properties')
        #                .get(r.PARAMS[SITE]['Xurl'])).encode('utf-8', 'replace')
        quake = {'utctime': time,
                 'place': place.encode('utf-8', 'replace'),
                 'magnitude': round(float(magnitude),2),
                 'latitude': float(latitude), 'longitude': float(longitude)}
        quakes.append(quake)
    return quakes


def pull_quakeml(url, SITE):
    from lxml import etree
    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError:
        return []
    try:
        tree = etree.parse(response).getroot()
    except etree.XMLSyntaxError:
        return []

    # the resources do not respect a standard for namespaces
    # e.g., ISC needs {'xmlns': 'http://quakeml.org/xmlns/quakeml/1.2'}
    # e.g., USGS needs {'xmlns': 'http://quakeml.org/xmlns/bed/1.2'}
    namespaces = tree.nsmap
    namespaces['xmlns'] = namespaces[None]
    namespaces.pop(None)

    quakes = []
    for e in tree.findall('xmlns:eventParameters/xmlns:event', namespaces):
        # magnitude
        magnitude=(e.find('xmlns:magnitude', namespaces)
                   .find('xmlns:mag', namespaces)
                   .find('xmlns:value', namespaces).text)
        # where
        origin=e.find('xmlns:origin', namespaces)
        place=(e.find('xmlns:description', namespaces)
                     .find('xmlns:text', namespaces).text)
        latitude=(origin.find('xmlns:latitude', namespaces)
                  .find('xmlns:value', namespaces).text)
        longitude=(origin.find('xmlns:longitude', namespaces)
                   .find('xmlns:value', namespaces).text)
        # when
        time=(origin.find('xmlns:time', namespaces)
              .find('xmlns:value', namespaces).text)
        quake = {'utctime':
                 datetime.datetime.strptime(time,
                                        r.PARAMS[SITE]['Xtimeformatquakeml']),
                 'place': place.encode('utf-8', 'replace'),
                 'magnitude': round(float(magnitude),2),
                 'latitude': float(latitude), 'longitude': float(longitude)}
        quakes.append(quake)
    return quakes



################################################################
#### TESTS
################################################################


def _test_geojson(site):
    TESTOPTIONS = ["&minmagnitude=5&starttime=2016-10-20T00:00:00&endtime=2016-11-0200:00:00",
                   "&minmagnitude=4.5&starttime=2016-10-30T00:00:00&endtime=2016-11-01T00:00:00",
                   "&minmagnitude=4.5&starttime=2014-01-30T00:00:00&endtime=2014-02-03T00:00:00",
                   "&minmagnitude=2.5&starttime=2014-01-30T00:00:00&endtime=2013-02-03T00:00:00",
                   "&starttime=2004-10-01T00:00:00&endtime=2007-11-02T00:00:00&orderby=time&minmagnitude=3.0&latitude=53.4054719&longitude=-2.9805391&maxradius=9.00900900901"]
    for testoptions in TESTOPTIONS:
        print testoptions
        baseurl = r.BASEURLS[site]
        fgeojson = r.PARAMS[site]['Xformatgeojson']
        url = baseurl+fgeojson+testoptions
        print "URL: " + url        
        geojsonquakes = pull_geojson(url, site)
        print "quakeml: " + str(len(geojsonquakes)) + " quakes"

        
def _test_qml(site):
    TESTOPTIONS = ["&minmagnitude=5&starttime=2016-10-20T00:00:00&endtime=2016-11-0200:00:00",
                   "&minmagnitude=4.5&starttime=2016-10-30T00:00:00&endtime=2016-11-01T00:00:00",
                   "&minmagnitude=4.5&starttime=2014-01-30T00:00:00&endtime=2014-02-03T00:00:00",
                   "&minmagnitude=2.5&starttime=2014-01-30T00:00:00&endtime=2013-02-03T00:00:00",
                   "&starttime=2004-10-01T00:00:00&endtime=2007-11-02T00:00:00&orderby=time&minmagnitude=3.0&latitude=53.4054719&longitude=-2.9805391&maxradius=9.00900900901"]
    for testoptions in TESTOPTIONS:
        print testoptions
        baseurl = r.BASEURLS[site]
        fquakeml = r.PARAMS[site]['Xformatquakeml']
        url = baseurl+fquakeml+testoptions
        print "URL: " + url
        qmlquakes = pull_quakeml(url, site)
        print "quakeml: " + str(len(qmlquakes)) + " quakes"
        
        
def _test_both(site):
    TESTOPTIONS = ["&minmagnitude=5&starttime=2016-10-20T00:00:00&endtime=2016-11-0200:00:00",
                   "&minmagnitude=4.5&starttime=2016-10-30T00:00:00&endtime=2016-11-01T00:00:00",
                   "&minmagnitude=4.5&starttime=2014-01-30T00:00:00&endtime=2014-02-03T00:00:00",
                   "&minmagnitude=2.5&starttime=2014-01-30T00:00:00&endtime=2013-02-03T00:00:00"]
    for testoptions in TESTOPTIONS:
        print testoptions
        baseurl = r.BASEURLS[site]
        fquakeml = r.PARAMS[site]['Xformatquakeml']
        fgeojson = r.PARAMS[site]['Xformatgeojson']
        geoquakes = pull_geojson(baseurl+fgeojson+testoptions, site)
        qmlquakes = pull_quakeml(baseurl+fquakeml+testoptions, site)
        print "geojson: " + str(len(geoquakes)) + " quakes"
        print "quakeml: " + str(len(qmlquakes)) + " quakes"
        print "SUCCESS" if (geoquakes == qmlquakes) else "FAIL"

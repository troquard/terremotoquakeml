RESOURCES = { 'us':
              {'name': "USGS",
               'baseurl': "http://earthquake.usgs.gov/fdsnws/event/1/query?"},
              'it':
              {'name': "INGV",
               'baseurl': "http://webservices.ingv.it/fdsnws/event/1/query?"},
              'fr':
              {'name': "RENASS",
               'baseurl': "http://renass.unistra.fr/fdsnws/event/1/query?"},
              'ch':
              {'name': "ETHZ",
               'baseurl': "http://arclink.ethz.ch/fdsnws/event/1/query?"},
              'uk':
              {'name': "ISC",
               'baseurl': "http://www.isc.ac.uk/fdsnws/event/1/query?"}
          }


BASEURLS = { 'USGS': "http://earthquake.usgs.gov/fdsnws/event/1/query?",
             'INGV': "http://webservices.ingv.it/fdsnws/event/1/query?",
             'RENASS': "http://renass.unistra.fr/fdsnws/event/1/query?",
             'ETHZ': "http://arclink.ethz.ch/fdsnws/event/1/query?",
             'ISC': "http://www.isc.ac.uk/fdsnws/event/1/query?" }

# see http://[domain]/fdsnws/event/1/application.wadl
PARAMS = { 'USGS':
           {'Xformatgeojson': "format=geojson",
            'Xformatquakeml': "format=xml",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "nummillisec",
            'Xtimeformatgeojson': "nummillisec",
            'Xtimeformatquakeml': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': "alert"},
           'INGV':
           {'Xformatgeojson': "format=geojson",
            'Xformatquakeml': "format=xml",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "%Y-%m-%dT%H:%M:%S.%f",
            'Xtimeformatgeojson': "%Y-%m-%dT%H:%M:%S.%f",
            'Xtimeformatquakeml': "%Y-%m-%dT%H:%M:%S.%f",
            'Xorderbytime': "&orderby=time",
            'Xurl': None,
            'Xalert': None },
           'RENASS':
           {'Xformatgeojson': "format=json",
            'Xformatquakeml': "format=qml",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xtimeformatgeojson': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xtimeformatquakeml': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': None },
           'ETHZ':
           {'Xformatgeojson': None,
            'Xformatquakeml': "format=xml",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': (None, "%Y-%m-%dT%H:%M:%S.%fZ"),
            'Xtimeformatgeojson': None,
            'Xtimeformatquakeml': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': None },
           'ISC':
           {'Xformatgeojson': None,
            'Xformatquakeml': "format=xml",
            'minmagnitude': "&minmagnitude=",
            'latitude': "&latitude=",
            'longitude': "&longitude=",
            'maxradius': "&maxradius=",
            'starttime': "&starttime=",
            'endtime': "&endtime=",
            'Xtimeformat': (None, "%Y-%m-%dT%H:%M:%S.%fZ"),
            'Xtimeformatgeojson': None,
            'Xtimeformatquakeml': "%Y-%m-%dT%H:%M:%S.%fZ",
            'Xorderbytime': "&orderby=time",
            'Xurl': "url",
            'Xalert': None }}
           


